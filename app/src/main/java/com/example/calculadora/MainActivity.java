package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    double resultado = 0, num1, num2;
    String operacion, signo, r, n1, n2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Declarar botón, R-vista general
        Button b0 = (Button) findViewById(R.id.button);
        Button b1 = (Button) findViewById(R.id.button1);
        Button b2 = (Button) findViewById(R.id.button2);
        Button b3 = (Button) findViewById(R.id.button3);
        Button b4 = (Button) findViewById(R.id.button4);
        Button b5 = (Button) findViewById(R.id.button5);
        Button b6 = (Button) findViewById(R.id.button6);
        Button b7 = (Button) findViewById(R.id.button7);
        Button b8 = (Button) findViewById(R.id.button8);
        Button b9 = (Button) findViewById(R.id.button9);
        Button bPunto = (Button) findViewById(R.id.buttonPunto);
        Button bBorrar = (Button) findViewById(R.id.buttonBorrar);

        Button bMas = (Button) findViewById(R.id.buttonMas);
        Button bMenos = (Button) findViewById(R.id.buttonMenos);
        Button bMult = (Button) findViewById(R.id.buttonMult);
        Button bDiv = (Button) findViewById(R.id.buttonDiv);
        Button bIgual = (Button) findViewById(R.id.buttonIgual);


        TextView tv1 = (TextView) findViewById(R.id.textViewNum);
        TextView tvResul = (TextView) findViewById(R.id.textViewResul);
        TextView tvSigno = (TextView) findViewById(R.id.textViewSigno);

        //Enviar el valor
        b0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Se toma el valor del TextView
                operacion = tv1.getText().toString();
                //A la variable se le agrega el valor del boton, para la operación
                operacion = operacion + "0";
                //Enviar el valor al TextView
                tv1.setText(operacion);
            }
        });
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + "1";
                tv1.setText(operacion);
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + "2";
                tv1.setText(operacion);
            }
        });
        b3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + "3";
                tv1.setText(operacion);
            }
        });
        b4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + "4";
                tv1.setText(operacion);
            }
        });
        b5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + "5";
                tv1.setText(operacion);
            }
        });
        b6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + "6";
                tv1.setText(operacion);
            }
        });
        b7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + "7";
                tv1.setText(operacion);
            }
        });
        b8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + "8";
                tv1.setText(operacion);
            }
        });
        b9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + "9";
                tv1.setText(operacion);
            }
        });
        bPunto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = tv1.getText().toString();
                operacion = operacion + ".";
                tv1.setText(operacion);
            }
        });
        bBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                operacion = "";
                tv1.setText(operacion);
                n1 = "";
                n2 = "";
                signo ="";
                tvResul.setText(operacion);
                tvSigno.setText(signo);
            }
        });
        bMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                n1 = tv1.getText().toString();
                num1 = Double.parseDouble(n1);
                signo = "+";
                tvSigno.setText(signo);
                tv1.setText("");

            }
        });
        bMenos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                n1 = tv1.getText().toString();
                num1 = Double.parseDouble(n1);
                signo = "-";
                tvSigno.setText(signo);
                tv1.setText("");

            }
        });
        bMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                n1 = tv1.getText().toString();
                num1 = Double.parseDouble(n1);
                signo = "x";
                tvSigno.setText(signo);
                tv1.setText("");

            }
        });
        bDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                n1 = tv1.getText().toString();
                num1 = Double.parseDouble(n1);
                signo = "/";
                tvSigno.setText(signo);
                tv1.setText("");

            }
        });

        bIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                n2  = tv1.getText().toString();
                num2 = Double.parseDouble(n2);
                if(signo == "+"){
                    resultado = num1 + num2;
                    tv1.setText(n1+" + "+n2+"=");
                    tvSigno.setText("");
                    r = String.valueOf(resultado);
                    tvResul.setText(r);
                }
                if(signo == "-"){
                    resultado = num1-num2;
                    tv1.setText(n1+" - "+n2+"=");
                    tvSigno.setText("");
                    r = String.valueOf(resultado);
                    tvResul.setText(r);
                }
                if(signo == "x"){
                    resultado = num1 * num2;
                    tv1.setText(n1+" x "+n2+"=");
                    tvSigno.setText("");
                    r = String.valueOf(resultado);
                    tvResul.setText(r);
                }
                if(signo == "/"){
                    resultado = num1 / num2;
                    tv1.setText(n1+" / "+n2+"=");
                    tvSigno.setText("");
                    r = String.valueOf(resultado);
                    tvResul.setText(r);

                }
            }
        });


    }
}